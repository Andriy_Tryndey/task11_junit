package com.andriy;

import com.andriy.tasks.Plateau;

import java.util.Arrays;
import java.util.Random;

public class Application {

    public static void main(String[] args) {
        int x = 100;
        Random r = new Random();
        Plateau plateau = new Plateau();
        int[] mas = plateau.inputIntegerToMas(20, 10, r);
        System.out.println(Arrays.toString(mas));

        plateau.findPlateau(mas);
    }

}
