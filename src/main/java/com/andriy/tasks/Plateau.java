package com.andriy.tasks;

import java.util.Arrays;
import java.util.Random;

/**
 * Find plateau in the array
 * End find start plateau and finish
 */
public class Plateau {
    public Random r;
    private int length;
    private int number;
    public int maxcounter = 0;
    public int startPlateau = 0;
    public int endPlateau = 0;
    public int[] mas;

    public Plateau() {
    }

    public Random getR() {
        return r;
    }

    public Plateau setR(Random r) {
        this.r = r;
        return this;
    }

    public int getLength() {
        return length;
    }

    public Plateau setLength(int length) {
        this.length = length;
        return this;
    }

    public int getNumber() {
        return number;
    }

    public Plateau setNumber(int number) {
        this.number = number;
        return this;
    }

    public int[] getMas() {
        return mas;
    }

    public Plateau setMas(int[] mas) {
        this.mas = mas;
        return this;
    }

    public int addNumber(int x, int y){
        int z;
        z = x + y;
        return z;
    }

    /**
     * method that fills the array
     * @param length
     * @param number
     * @param r
     * @return
     */
    public int[]  inputIntegerToMas(int length, int number, Random r){
        mas = new int[length];
        for (int i = 0; i < mas.length; i++) {
            mas[i] = r.nextInt(number);
        }
        return mas;
    }

    /**
     * The longest plateau method
     * @param mas
     */
    public void findPlateau(int[] mas){
        int counter = 1;
        for (int i = 0, j = i + 1; i < mas.length - 1; i++, j++) {
            if (mas[i] == mas[j]) {
                counter++;
            } else {
                counter = 1;
            }
            if (counter > maxcounter) {
                if ((i + 1) == (mas.length - 1) || mas[i + 1] > mas[i + 2]) {
                    endPlateau = i + 1;
                    maxcounter = counter;
                    startPlateau = endPlateau - maxcounter + 1;
                }
            }
        }
        System.out.println("startPlateau = " + startPlateau);
        System.out.println("endPlateau = " + endPlateau);
        System.out.println("maxcounter = " + maxcounter);
    }

    @Override
    public String toString() {
        return "Plateau{" +
                "r=" + r +
                ", length=" + length +
                ", number=" + number +
                ", mas=" + Arrays.toString(mas) +
                '}';
    }
}
