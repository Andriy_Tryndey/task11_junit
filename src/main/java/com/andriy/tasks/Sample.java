package com.andriy.tasks;

public class Sample {
    private String name;
    private int value1;
    private int value2;
    private int value3;

    static final String finStr1 = "foo1";
    protected static final String finStr2 = "foo2";

    public Sample(String name) {this.name = name;}

    public int addToValue1(int value){return value + value1;}

    public void calcValue1(){value3 = value1 + value2;}

    public String getName() {
        return name;
    }

    public Sample setName(String name) {
        this.name = name;
        return this;
    }

    public int getValue1() {
        return value1;
    }

    public Sample setValue1(int value1) {
        this.value1 = value1;
        return this;
    }

    public int getValue2() {
        return value2;
    }

    public Sample setValue2(int value2) {
        this.value2 = value2;
        return this;
    }

    public int getValue3() {
        return value3;
    }

    public Sample setValue3(int value3) {
        this.value3 = value3;
        return this;
    }

    public static String getFinStr1() {
        return finStr1;
    }

    public static String getFinStr2() {
        return finStr2;
    }

    public void getAbbracadabra(int value) throws Exception{
        if (value == 1) {
            throw new Exception("Abbracadabra");
        }
    }
}
