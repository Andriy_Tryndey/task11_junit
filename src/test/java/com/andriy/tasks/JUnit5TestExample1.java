package com.andriy.tasks;

import org.junit.Test;
import org.junit.jupiter.api.*;

public class JUnit5TestExample1 {
    @BeforeAll
    static void initAll(){
        System.out.println("@BeforeAll - Run before all methods once");
    }
    @BeforeEach
    public void init(){
        System.out.println("@BeforeEach - Run before each test methods");
    }
    @DisplayName("First test")
    @Test
    public void testMethod1(){
        System.out.println("Test method 1");
    }
    @Test
    @Disabled
    public void testMethod2(){
        System.out.println("Test method 2");
    }
    @Test
    public void testMethod3(){
        System.out.println("Test method 3");
    }
    @AfterEach
    public void tearDown() {
        System.out.println("@AfterEach - Run after each test method");
    }
    @AfterAll
    static void tearDonAll(){
        System.out.println("@AfterAll - Run after all test methods once");
    }
}
