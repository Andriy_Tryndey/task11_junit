package com.andriy.tasks;

import org.junit.Test;

import static org.junit.Assume.assumeTrue;

public class JunitTest2 {
    private boolean condition = false;
    @Test
    void testMethod1() {
        assumeTrue(condition);
        System.out.println("  Test method 1");
    }
    @Test
    void testMethod2() {
        assumeTrue(condition);
        System.out.println("  Test method 2");
    }
    @Test
    void testMethod3() {
        assumeTrue(condition);
        System.out.println("  Test method 3");
    }
}
