package com.andriy.tasks;

import org.junit.jupiter.api.Test;

import static org.junit.Assert.fail;
import static org.junit.Assume.assumeFalse;
import static org.junit.Assume.assumeTrue;

public class JUnit5TestExample2 {
    private boolean condition = false;

    @Test
    void testMethod1() {
        assumeTrue(condition);
        System.out.println("Test method 1");
    }
    @Test
    void testMethod2(){
        assumeFalse(condition);
        System.out.println("Test method 2");
    }
    @Test
    void testMethod3() {
        fail("Hello! :)");
        System.out.println("Test method 3");
    }
}
