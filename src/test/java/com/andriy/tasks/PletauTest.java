package com.andriy.tasks;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import java.util.Random;

import static junit.framework.TestCase.assertTrue;

public class PletauTest {
    private final static boolean bug = true;
    private static int count = 0;

    @BeforeAll
    public static void beforeClass(){
        System.out.println("Count beforeClass is : " + count);
    }

    @BeforeEach
    public void beforeTest(){
        System.out.println("Count before is : " + count);
    }

    @AfterEach
    public void afterTest(){
        System.out.println("Count after is : " + count);
    }

    @BeforeAll
    public static void afterClass(){
        System.out.println("Count afterClass is : " + count);
    }

    @Test
    public void testAddNumber(){
        count++;
        Plateau plateau = new Plateau();
        int number = plateau.addNumber(20, 10);
        assertTrue("Result :" + number + "!=30", number == 30);
    }

    @Test
    public void test1FindPlateau(){
        count++;
        Random r = new Random();
        Plateau plateau = new Plateau();
        int[] mas = plateau.inputIntegerToMas(20, 10, r);
        plateau.findPlateau(mas);
        int maxcounter = plateau.maxcounter;
        assertTrue("Result : " + maxcounter + "> 1", maxcounter > 1);
    }
}
